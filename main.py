# Example:
# https://steamcommunity.com/market/multisell?appid=440&contextid=2&items[]=Scream%20Fortress%20XII%20War%20Paint%20Case&qty[]=0
BASE_URL="https://steamcommunity.com/market/multisell?"

# Replace space with space understandable to browser
def fixUrl(url: str) -> str:
    return url.replace(' ', '%20')

# Add a parameter to the url
def addProp(baseUrl: str, propName: str, prop: str) -> str:
    return baseUrl + '&' + fixUrl(propName) + '=' + fixUrl(prop)

# Add an array to the url
def addArray(baseUrl: str, arrName: str, arr: list) -> str:
    for prop in arr:
        # "[]=" - Indicates that the parameter is an array.
        baseUrl += '&' + fixUrl(arrName) + "[]=" + fixUrl(prop)
    return baseUrl

def main():
    appid = input("Enter appID (440): ")
    if appid == "":
        appid = "440"
    url = addProp(BASE_URL, "appid", appid)
    url = addProp(url, "contextid", "2") # IDK what it is

    items = []
    _inp = input("Enter the list of items ending with an empty line:\n")
    while _inp != "":
        items.append(_inp)
        _inp = input()
    
    url = addArray(url, "items", items)
    url = addArray(url, "qty", ["0"]) # IDK what it is too

    print("Url:",url)

if __name__ == "__main__":
    main()
